describe("Spotify login functionality", () => {
  it("UC - 1: Test Login form with empty credentials", async () => {
    browser.url("https://open.spotify.com");
    let logInButton = $(
      "//*[@id='main']/div/div[2]/div[3]/header/div[5]/button[2]"
    );
    await logInButton.click();

    let logInUsername = await $("#login-username");
    let logInPassword = await $("#login-password");

    await logInUsername.setValue("login");
    await logInPassword.setValue("password");

    await logInUsername.clearValue();
    await logInPassword.clearValue();

    /**
     * After this stage I've got problem because I don't know how to trigger error messages.
     * I think the problem that aria-invalid in login form is always "false" when I have ran tests.
     * Manually it's working and value change to "true". So I stuck at this point.
     */

    let logInUsernameButton = await $("#login-button");

    await logInUsernameButton.click();

    let logInErrorMessage = await $("//*[@id='username-error']/span/p");
    let passwordErrorMessage = await $("#password-error > span");

    expect(await logInErrorMessage.getText()).toEqual(
      "Please enter your Spotify username or email address."
    );
    expect(await passwordErrorMessage.getText()).toEqual(
      "Please enter your password."
    );
  });

  it("UC - 2: Test Login form with incorrect credentials", async () => {
    browser.url("https://open.spotify.com");
    let logInButton = $(
      "//*[@id='main']/div/div[2]/div[3]/header/div[5]/button[2]"
    );
    await logInButton.click();

    let logInUsername = await $("#login-username");
    let logInPassword = await $("#login-password");

    await logInUsername.setValue("login");
    await logInPassword.setValue("password");

    let logInUsernameButton = await $("#login-button");

    await logInUsernameButton.click();

    let logInErrorMessage = await $(
      "//*[@id='root']/div/div[2]/div/div/div[1]/div/span"
    );

    expect(await logInErrorMessage.getText()).toEqual(
      "Incorrect username or password."
    );
  });

  it("UC - 3: Test Login form with correct credentials", async () => {
    browser.url("https://open.spotify.com");
    let logInButton = $(
      "//*[@id='main']/div/div[2]/div[3]/header/div[5]/button[2]"
    );
    await logInButton.click();

    let logInUsername = await $("#login-username");
    let logInPassword = await $("#login-password");

    await logInUsername.setValue("yurii@fastmail.com");
    await logInPassword.setValue("Abc14789+");

    let logInUsernameButton = await $("#login-button");

    await logInUsernameButton.click();
    await browser.pause(500);

    let avatar = await $(".tp8rO9vtqBGPLOhwcdYv");
    let avatarTitle = await avatar.getAttribute("title");
    let expectedTitle = "Yurii";

    await expect(avatarTitle).toEqual(expectedTitle);
  });
});
